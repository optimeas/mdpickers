# mdPickers
Material Design date/time pickers built with Angular Material and Moment.js

Note: This repository was forked from [alenaksu/mdPickers](https://github.com/alenaksu/mdPickers)

With version 0.8.0 a support for timezones has been added.


## Requirements

* [moment.js](http://momentjs.com/)
* [moment-timezone.js](http://momentjs.com/timezone)
* [AngularJS](https://angularjs.org/)
* [Angular Material](https://material.angularjs.org/)

## Using mdPickers

Install via Bower:

```bash
bower install mdPickers
```

Use in Angular:
```javascript
angular.module( 'YourApp', [ 'mdPickers' ] )
  .controller("YourController", YourController );
```

## Building mdPickers

First install or update your local project's __npm__ tools:

```bash
# First install all the npm tools:
npm install

# or update
npm update
```

Then run the default gulp task:

```bash
# builds all files in the `dist` directory
gulp
```
