/* global moment, angular */

module.factory("dateChangeService", [
    function () {

        return {
            changeDayOfMonth: changeDayOfMonth,
            putAllowedDate: putAllowedDate,
            getAllowedDateString: getAllowedDateString
        };

        var allowedDatesMap = {};

        function changeDayOfMonth(pickerId, date) {
            if (!allowedDatesMap[pickerId]) {
                return date;
            }

            var newDate = moment(date);
            var newDayOfMonth = newDate.date();
            var allowedDayOfMonth = allowedDatesMap[pickerId].dayOfMonth;

            if (newDayOfMonth - allowedDayOfMonth === 1) {
                newDate.date(allowedDayOfMonth + 1);
            } else if (newDayOfMonth - allowedDayOfMonth === -1) {
                newDate.date(allowedDayOfMonth - 1);
            }

            return newDate.toDate();
        }

        function putAllowedDate(pickerId, date) {
            if (!allowedDatesMap) {
                allowedDatesMap = {};
            }

            var momentDate = moment(date);
            if (momentDate.isValid()) {
                allowedDatesMap[pickerId] = {
                    dateString: momentDate.format("YYYY-MM-DD"),
                    dayOfMonth: momentDate.date()
                };
            }
        }

        function getAllowedDateString(pickerId) {
            return allowedDatesMap[pickerId] ? allowedDatesMap[pickerId].dateString : "";
        }
    }
]);